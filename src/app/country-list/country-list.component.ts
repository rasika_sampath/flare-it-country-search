
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, tap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { CountryListService } from './country-list-service';
import { CountryModel } from './country-model';


@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit {
  selectedCountry: CountryModel;
  searching = false;
  searchFailed = false;
  countryList: CountryModel[];
  searchedCountryList: CountryModel[] = [];

  constructor(private _service: CountryListService) { }

  // retrieve country list at onInit
  ngOnInit() {
    this.loadCountryList();
  }

  loadCountryList() {
    this._service.search().subscribe(searchResult => {
      this.countryList = searchResult;
    });
  }

  formatter(x: CountryModel) {
    return x.name + '-' + x.alpha3Code;
  }

  //https://ng-bootstrap.github.io/#/components/typeahead/examples
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.countryList.filter(country => country.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  countryClicked(selection) {
    this.selectedCountry = selection.item;
    this.searchedCountryList.push(selection.item);
  }

  // prepare search data as per business rules
  // reverse the order and show only distinct
  getSelectedCountryList() {
    this.searchedCountryList.reverse();
    return [...new Set(this.searchedCountryList)];
  }

  setSelectedCountry(selectedCountryFromHistory) {
    this.selectedCountry = selectedCountryFromHistory;
  }

}
