import { TestBed } from '@angular/core/testing';

import { CountryListService } from './country-list-service';
import { APP_BASE_HREF } from '@angular/common';

describe('CountryListServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: APP_BASE_HREF, useValue: '/' }
    ]
  }));

  it('should be created', () => {
    const service: CountryListService = TestBed.get(CountryListService );
    expect(service).toBeTruthy();
  });
});
