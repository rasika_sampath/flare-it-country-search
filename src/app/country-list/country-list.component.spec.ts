import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { CountryListComponent } from './country-list.component';
import { CountryInformationComponent } from '../country-information/country-information.component';
import { CountrySearchHisotryComponent } from '../country-search-hisotry/country-search-hisotry.component';
import { CountryListService } from './country-list-service';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { APP_BASE_HREF } from '@angular/common';
import { NgbModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';

describe('CountryListComponent', () => {
  let component: CountryListComponent;
  let fixture: ComponentFixture<CountryListComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CountryListComponent,
        CountryInformationComponent, CountrySearchHisotryComponent],
      imports: [NgbTypeaheadModule, NgbModule],
      providers: [
        {
          provide: CountryListService,
          useClass: CountryListServiceMock
        },
        {
          provide: APP_BASE_HREF, useValue: '/'
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryListComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});


export class CountryListServiceMock {

  public search() {
    const data: any = require('./mock-data.json');

    return of(data);
  }
}