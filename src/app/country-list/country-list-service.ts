import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CountryModel } from './country-model';

@Injectable({
  providedIn: 'root'
})
export class CountryListService {

  constructor(private http: HttpClient) { }

  countrylistUrl = 'https://restcountries.eu/rest/v2/all';


  search(): Observable<CountryModel[]> {
    return this.http
      .get(this.countrylistUrl).pipe(
        map((entries: CountryModel[]) => entries)
      );
  }


}
