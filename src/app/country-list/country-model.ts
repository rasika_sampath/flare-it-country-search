// auto generated from http://www.json2ts.com/

export class Currency {
    code: string;
    name: string;
    symbol: string;
}

export class Language {
    iso639_1: string;
    iso639_2: string;
    name: string;
    nativeName: string;
}

export class Translations {
    de: string;
    es: string;
    fr: string;
    ja: string;
    it: string;
    br: string;
    pt: string;
    nl: string;
    hr: string;
    fa: string;
}

export class RegionalBloc {
    acronym: string;
    name: string;
    otherAcronyms: any[];
    otherNames: any[];
}

export class CountryModel {
    constructor() {

    }
    public name: string;
    public topLevelDomain: string[];
    public alpha2Code: string;
    public alpha3Code: string;
    public callingCodes: string[];
    public capital: string;
    public altSpellings: string[];
    public region: string;
    public subregion: string;
    public population: number;
    public latlng: number[];
    public demonym: string;
    public area: number;
    public gini: number;
    public timezones: string[];
    public borders: string[];
    public nativeName: string;
    public numericCode: string;
    public currencies: Currency[];
    public languages: Language[];
    public translations: Translations;
    public flag: string;
    public regionalBlocs: RegionalBloc[];
    public cioc: string;
}



