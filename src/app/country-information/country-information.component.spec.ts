import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CountryModel } from '../country-list/country-model';
import { CountryInformationComponent } from './country-information.component';
import { APP_BASE_HREF } from '@angular/common';

describe('CountryInformationComponent', () => {
  let component: CountryInformationComponent;
  let fixture: ComponentFixture<CountryInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CountryInformationComponent],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryInformationComponent);
    component = fixture.componentInstance;
    component.countryModel = new CountryModel();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
