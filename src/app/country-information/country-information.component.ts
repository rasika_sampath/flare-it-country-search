import { Component, OnInit, Input } from '@angular/core';
import { CountryModel } from '../country-list/country-model';

@Component({
  selector: 'app-country-information',
  templateUrl: './country-information.component.html',
  styleUrls: ['./country-information.component.css']
})
export class CountryInformationComponent implements OnInit {

  @Input() countryModel: CountryModel;
  constructor() { }

  ngOnInit() {
  }

}
