import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrySearchHisotryComponent } from './country-search-hisotry.component';
import { APP_BASE_HREF } from '@angular/common';

describe('CountrySearchHisotryComponent', () => {
  let component: CountrySearchHisotryComponent;
  let fixture: ComponentFixture<CountrySearchHisotryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CountrySearchHisotryComponent],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountrySearchHisotryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
