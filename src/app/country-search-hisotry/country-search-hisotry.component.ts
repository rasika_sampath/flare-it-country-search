import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CountryModel } from '../country-list/country-model';

@Component({
  selector: 'app-country-search-hisotry',
  templateUrl: './country-search-hisotry.component.html',
  styleUrls: ['./country-search-hisotry.component.css']
})
export class CountrySearchHisotryComponent implements OnInit {

  @Input() searchedCountryList: CountryModel[];
  @Output() countrySelectedFromHistory = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  countryClicked($event, country) {
    this.countrySelectedFromHistory.emit(country);
    $event.preventDefault();
  }


}
